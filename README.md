## About

Code written by Bjørn Jensen for the `COVID-19-CTRL` joint project between
The Technical University of Denmark (DTU) and Aalborg University (AAU).

To cite, cite this article:    
*Efficient Uncertainty Quantification and Variance-Based Sensitivity Analysis in Epidemic Modelling Using Polynomial Chaos (2022) - Jensen B., Engsig-Karup A.P., Knudsen K.* | doi:https://doi.org/10.1051/mmnp/2022014

## How to generate figures and numbers from article

### Case Peaks
**Run** script `peaks/example_case_peaks.m`  
**Run** script `peaks/example_mcmc.m`

### Case Superspreaders
**Run** script `superspreaders/fit_parameters.m`  
**Run** script `superspreaders/simulation.m`  
**Run** script `superspreaders/simulation_uq.m`  

## Data file
**DATAFILE**: `superspreaders/Newly_admitted_over_time.csv`   
*Source*: [SSI.DK](www.ssi.dk), retrieved June 14th, 2020.

## License

    MIT License

    Copyright (c) 2020 Bjørn Jensen

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
