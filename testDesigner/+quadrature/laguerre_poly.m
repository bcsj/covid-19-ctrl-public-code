% y = laguerre(n,X)
%
% Evaluates the laguerre polynomials L_0,L_1,...,L_n
% at the points X.
%
% Author information
%   Copyright (c) 2020 Bjørn Jensen
%
function y = laguerre_poly(n,x)

x = x(:);
coef = [];
if numel(n) > 1, coef = n(:); n = numel(n)-1; end
y = zeros(numel(x),n+1);
y(:,1) = 1;
if n == 0, return; end

y(:,2) = 1-x;
if n == 1
    if ~isempty(coef)
        y = y*coef;
    end
    return
end

for i = 3:n+1
    y(:,i) = (2*i-3-x)/(i-1).*y(:,i-1) - (i-2)/(i-1)*y(:,i-2);
end
if ~isempty(coef)
    y = y*coef;
end

end