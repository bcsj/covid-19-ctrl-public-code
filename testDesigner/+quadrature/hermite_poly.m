% y = hermite_poly(n,X)
%
% Evaluates the legendre polynomials H_0,H_1,...,H_n
% at the points X.
%
% Author information
%   Copyright (c) 2020 Bjørn Jensen
%
function y = hermite_poly(n,x)

x = x(:);
coef = [];
if numel(n) > 1, coef = n(:); n = numel(n)-1; end
y = zeros(numel(x),n+1);
y(:,1) = 1;
if n == 0, return; end

y(:,2) = x;
if n == 1
    if ~isempty(coef)
        y = y*coef;
    end
    return
end

for i = 3:n+1
    y(:,i) = x.*y(:,i-1) - (i-2)*y(:,i-2);
end
if ~isempty(coef)
    y = y*coef;
end