%
% Author information
%   Copyright (c) 2020 Bjørn Jensen
%
function [xi,wg] = quadrature(qtype, degree)

switch (lower(qtype))
    case 'hermite'
        [xi,wg] = quadrature.hermite_quad(degree);
    case 'legendre'
        [xi,wg] = quadrature.legendre_quad(degree);
    case 'laguerre'
        [xi,wg] = quadrature.laguerre_quad(degree);
    otherwise
        warning('quadrature type not recogniced');
        xi = NaN;
        wg = NaN;
end

end