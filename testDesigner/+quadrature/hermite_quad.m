%
% Computes the quadrature points XI and weights WG
% for the n'th order quadrature.
%
% XI are the roots of H_n(x)
%
% Based on the Numpy implementation, source:
% https://github.com/numpy/numpy/blob/v1.20.0/numpy/polynomial/hermite.py#L1558-L1622
%
function [xi,wg] = hermite_quad(n)

A = hermite_matrix(n);
xi = sort(eig(A));
xi = (xi - xi(end:-1:1))/2;

fm = herm_norm(n-1, xi);
fm = fm/max(abs(fm(:)));
w = 1./(fm.*fm);
w = (w + w(end:-1:1))/2;
wg = w*sqrt(2*pi)/sum(w(:));

end

% =======================================================
% =======================================================
function A = hermite_matrix(n)
% A = hermite_matrix(n)
%
% The companion matrix for the Hermite polynomial
% of degree n, H_n. 
% Used for computing the roots of H_n.
%

A = diag(sqrt(1:n-1),1) + diag(sqrt(1:n-1),-1);
end

% =======================================================
% =======================================================
function y = herm_norm(n,x)
% Stolen from numpy source code
if n == 0
    y = ones(size(x))/sqrt(sqrt(2*pi));
    return
end
c0 = 0;
c1 = 1/sqrt(sqrt(2*pi));
for i = 1:n-1
    tmp = c0;
    c0 = -c1*sqrt((n-1)/n);
    c1 = tmp + c1.*x.*sqrt(1/n);
    n = n - 1;
end
y = c0 + c1.*x;
end