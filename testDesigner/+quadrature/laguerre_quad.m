%
% Computes the quadrature points XI and weights WG
% for the n'th order quadrature.
%
% XI are the roots of L_n(x)
%
% Based on the Numpy implementation, source:
% https://github.com/numpy/numpy/blob/v1.20.0/numpy/polynomial/laguerre.py#L1511-L1572
%
function [xi,wg] = laguerre_quad(n)

A = laguerre_matrix(n);
xi = sort(eig(A));

y = laguerre_poly(n,xi);
c = laguerre_deriv(n);
dy = y(:,1:n)*c;

% improve roots by one Newton step
xi = xi - y(:,end)./dy;

fm = y(:,end-1);
df = dy;
fm = fm/max(abs(fm(:)));
df = df/max(abs(df(:)));
w = 1./(fm.*df);
wg = w/sum(w(:));

end

% =======================================================
% =======================================================
function coef = laguerre_deriv(n)
% Computes the coefficient of the expansion of P_n' in
% the basis L_0,L_1,...,L_{n-1}
%
% To evaluate L_n' at x call 
%   laguerre_poly(laguerre_deriv(n),x)
%

coef = -ones(n,1);
end

function y = laguerre_poly(n,x)
% y = laguerre(n,X)
%
% Evaluates the laguerre polynomials L_0,L_1,...,L_n
% at the points X.
%
x = x(:);
y = zeros(numel(x),n+1);
y(:,1) = 1;
if n == 0, return; end
y(:,2) = 1-x;
if n == 1, return; end
for i = 3:n+1
    y(:,i) = (2*i-3-x)/(i-1).*y(:,i-1) - (i-2)/(i-1)*y(:,i-2);
end
end

function A = laguerre_matrix(n)
% A = laguerre_matrix(n)
%
% The companion matrix for the Hermite polynomial
% of degree n, L_n. 
% Used for computing the roots of L_n.
%

A = diag(2*(1:n)-1) + diag(-(1:n-1),1) + diag(-(1:n-1),-1);
end