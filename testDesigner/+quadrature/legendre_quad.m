%
% Computes the quadrature points XI and weights WG
% for the n'th order quadrature.
%
% XI are the roots of P_n(x)
%
% Based on the Numpy implementation, source:
% https://github.com/numpy/numpy/blob/v1.20.0/numpy/polynomial/legendre.py#L1525-L1590
%
function [xi,wg] = legendre_quad(n)

A = legendre_matrix(n);
xi = sort(eig(A));
xi = (xi - xi(end:-1:1))/2;

y = legendre_poly(n-1,xi);
c = legendre_deriv(n);
dy = y*c;

fm = y(:,end);
df = dy;
fm = fm/max(abs(fm(:)));
df = df/max(abs(df(:)));
w = 1./(fm.*df);
w = (w + w(end:-1:1))/2;
wg = 2*w/sum(w(:));

end

% =======================================================
% =======================================================
function coef = legendre_deriv(n)
% Computes the coefficient of the expansion of P_n' in
% the basis P_0,P_1,...,P_{n-1}
%
% To evaluate P_n' at x call 
%   legendre_poly(legendre_deriv(n),x)
%
coef = zeros(n,1);
coef(end:-2:1) = 2*((n-1):-2:(n-2*ceil(n/2)))+1;

end

% =======================================================
% =======================================================
function y = legendre_poly(n,x)
% y = legendre_poly(n,X)
%
% Evaluates the legendre polynomials P_0,P_1,...,P_n
% at the points X.
%
x = x(:);
y = zeros(numel(x),n+1);
y(:,1) = 1;
if n == 0, return; end
y(:,2) = x;
if n == 1, return; end
for i = 3:n+1
    y(:,i) = (2*i-3)/(i-1)*x.*y(:,i-1) - (i-2)/(i-1)*y(:,i-2);
end
end

% =======================================================
% =======================================================
function A = legendre_matrix(n)
% A = legendre_matrix(n)
%
% The companion matrix for the legendre polynomial
% of degree n, P_n. 
% Used for computing the roots of P_n.
%

A = diag((1:n-1)./(1:2:2*(n-1)),1) + diag((1:n-1)./(3:2:2*n),-1);
end