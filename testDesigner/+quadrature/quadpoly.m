%
% Author information
%   Copyright (c) 2020 Bjørn Jensen
%
function y = quadpoly(qtype, degree, x)

switch (lower(qtype))
    case 'hermite'
        y = quadrature.hermite_poly(degree, x);
    case 'legendre'
        y = quadrature.legendre_poly(degree, x);
    case 'laguerre'
        y = quadrature.laguerre_poly(degree, x);    
    otherwise
        warning('quadrature type not recogniced');
        xi = NaN;
        wg = NaN;
end

end