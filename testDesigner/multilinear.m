% T = multilinearaction(T,A,ind)
% T = multilinearaction(T,A1,A2,...,An,ind)
% 
% Computes for ind = [i1,i2,i3,...,iN]
%
%   SUM(T[i1,..,iN].*A)
%

%
% MIT License
% 
% Copyright (c) 2020 Bjørn Jensen
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
function T = multilinear(T,varargin)

ind = varargin{end};
varargin = varargin(1:end-1);

if numel(varargin) == 1
    A = varargin{1};
    T = multilinear_single(T,A,ind);
    return
end

for i = 1:numel(varargin)
    A = varargin{i};
    n = dim(A);
    T = multilinear_single(T,A,ind(1:n));
    ind = ind(n+1:end)-n;
end

end

function T = multilinear_single(T,A,ind)
    idx = [ind,custom_setdiff(1:ndims(T),ind)];
    T = permute(T,idx);
    
    sz = size(T); m = dim(T); n = numel(ind);
    if m > n
        T = reshape(T,[prod(sz(1:n)),sz(n+1:end)]);
    elseif n > 1
        T = reshape(T,[prod(sz(1:n)),1]);
    else
        T = reshape(T,[max(sz),1]);
    end
    if m > n
        T = reshape(sum(T .* A(:),1),[sz(n+1:end),1]);
    else
        T = sum(T .* A(:),1);
    end
end

function C = custom_setdiff(A,B)
    % setdiff is slow
    idx = true(size(A));
    idx(B) = 0;
    C = A(idx);
end

function n = dim(A)
    n = ndims(A);
    if n == 2
        sz = size(A);
        if any(sz==1)
            n = 1;
        end
    end
end


