% [moments, sobol, coefX] = testdesigner(modelfun, distributions)
%
% Outputs:
%   moments : the moments of the output of MODELFUN
%   sobol   : the sobol symbols for MODELFUN
%   coefX   : the polynomial chaos expansion coefficients
%
%   gamma   : polynomial normalization coefficients
%       X   : computed samples
%
% Inputs:
%   modelfun : function_handle with parameter inputs A,B,C,... Should
%              return a scalar or vector
%
%   distributions : cell array with distribution properties for A,B,C,...
%       example cell for input parameters (A,B)
%           {
%               {'normal',    mean, var, quad_deg},
%               {'exponential', rate, quad_deg},
%           }
%
%       supported distributions (parameters): 
%           normal:      (mean, variance)
%           lognormal:   (mean, variance)
%           uniform:     (lowerbound, upperbound)
%           exponential: (rate)
%
%   options : optional parameter options
%       nMoments : integer (default: 2)
%                  Denotes the number of returned moments; more moments
%                  require higher order of quadratures.
%
%       lvlSobol : integer/scalar (0;1)/'all' (default: 3)
%                  An integer denotes the amount of parameter combinations
%                  returned. A scalar between 0 and 1 denotes the
%                  percentage of variance wanted accounted for. 'all'
%                  returns all symbols.
%
%
% Code written by Bjørn Jensen for the COVID-19-CTRL joint project between
% The Technical University of Denmark (DTU) and Aalborg University (AAU).
%
% To cite, cite this article:
%
%     Efficient Uncertainty Quantification of  Predictions using Polynomial 
%     Chaos in Epidemic Modeling (2021) - Jensen B., Engsig-Karup A.P., 
%     Knudsen K.
%

%
% MIT License
% 
% Copyright (c) 2020 Bjørn Jensen
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
function [moments, sobol, coefX, gamma, X] = testdesigner(modelfun, distributions, varargin)

% Check inputs
inputchk(modelfun, distributions);
options = inputparse(varargin);

% Initialize variables
ndist = length(distributions);
M     = zeros(1,ndist);
inp   = cell(1,ndist);
wg    = cell(1,ndist);
hi    = cell(1,ndist);
gamma = cell(1,ndist);

% Map distribution inputs to correpsonding quadratures
for i = 1:ndist
    [inp{i},wg{i},hi{i},gamma{i}] = dist2quad(distributions{i});
    M(i) = numel(inp{i});
end

% Initial model evaluation to get shape of output
ii = ones(size(M));
p = arrayfun(@(x,j) x{1}(ii(j)), inp, 1:ndist, 'UniformOutput', false);
X0 = modelfun(p{:});
N = numel(X0);
X = zeros(N,prod(M));
X(:,1) = X0;
ii = incr(ii,M);

% Evalute model
for i = 2:prod(M)
    p = arrayfun(@(x,j) x{1}(ii(j)), inp, 1:ndist, 'UniformOutput', false);
    X(:,i) = modelfun(p{:});
    ii = incr(ii,M);
end
X = reshape(X,[N,M]);

% Compute polynomial chaos expansion coefficients
coefX = zeros(N,prod(M));
ii = ones(size(M));
for i = 1:prod(M)
    p = arrayfun(@(j) wg{j}.*hi{j}(:,ii(j))/gamma{j}(ii(j)),1:ndist,'UniformOutput',false);
    coefX(:,i) = multilinear(X, p{:}, 2:ndist+1);
    ii = incr(ii,M);
end

% Compute moments
moments = cell(1,options.nMoments);
% Mean
mu = coefX(:,1);
moments{1} = mu;

% Variance
s = coefX;
s(:,1) = 0;
s = reshape(s,[N,M]);
p = arrayfun(@(x) x{1}/x{1}(1), gamma, 'UniformOutput', false);
sig2 = multilinear(s.^2, p{:}, 2:ndist+1);
moments{2} = sig2;

% Skew + kurtosis + more ...
p = arrayfun(@(i) wg{i}/gamma{i}(1), 1:ndist, 'UniformOutput', false);
for i = 3:numel(moments)
    moments{i} = multilinear(((X - mu)./sqrt(sig2)).^i,p{:},2:ndist+1);
end

% Don't compute Sobol symbols if they are not requested
if nargout < 2, return; end

% Compute Sobol symbols
sobol = sobolsymbols(coefX, N, M, ndist, gamma, sig2, options.lvlSobol);

end

% =================== SUPPORT FUNCTIONS ===================================
% =================== SUPPORT FUNCTIONS ===================================
% =================== SUPPORT FUNCTIONS ===================================

function sobol = sobolsymbols(coefX,N,M,ndist,gamma,var,level)

s = coefX;
s = reshape(s,[N,M]);
nsymb = 2^ndist-1;

ind = 1:ndist;
idx0 = cell(size(ind));
[subsets,key] = powerset(ind);

fraclvl = 1;
trunclvl = numel(M);
if ~ischar(level) % not 'all'
    if level < 1
        fraclvl = level;
    else
        trunclvl = level;
    end
end

sobol = zeros(N,nsymb);
tot = zeros(N,1);
for i = 1:nsymb-1
    if all(tot > fraclvl) || numel(subsets{i}) > trunclvl, break; end
    U = subsets{i};
    idx = custom_setdiff_idx(ind,U);
    idx0(:) = {':'};
    idx0(idx) = {1};
    Q = better_squeeze(s(:,idx0{:}));
    if size(s,1) == 1 % unfold if there is only one parameter
        Q = reshape(Q,[1,size(Q)]);
    end
    p = arrayfun(@(i) gamma{i}/gamma{i}(1), U, 'UniformOutput', false);
    Q(:,1) = 0;
    sobol(:,i) = multilinear(Q.^2,p{:},2:numel(U)+1)./var;
    u = powerset(U);
    for j = 1:numel(u)-1
        idx = key(sum(2.^(u{j}-1))); % replaces: idx = find_idx(u{j},subsets);
        sobol(:,i) = sobol(:,i) - sobol(:,idx);
    end
    tot = tot + sobol(:,i);
end

if ischar(level) || nsymb <= trunclvl % 'all'
    sobol(:,nsymb) = 1 - sum(sobol(:,1:nsymb-1),2);
else
    sobol = sobol(:,1:i);
end

end

function idx = custom_setdiff_idx(A,B)
    % setdiff is slow
    idx = true(size(A));
    idx(B) = 0;
end

function A = better_squeeze(A)
% Always squeeze 1-dimensional stuff to column vector (for consistency!)
A = squeeze(A);
if dim(A) == 1, A = A(:); end
end
function n = dim(A)
% like ndims, but returns 1 for vectors
n = ndims(A);
if n == 2
    sz = size(A);
    if any(sz==1)
        n = 1;
    end
end
end

function ii = incr(ii,M)
% multiindex incrementer
for i = 1:numel(M)
    ii(i) = ii(i) + 1;
    if i > 1, ii(i-1) = 1; end
    if ii(i) <= M(i), return, end
end
end

function [pU,key] = powerset(U)
% computes subsets of U
n = numel(U);
S = dec2bin(0:2^n-1) - '0';
[~,idx] = sort(sum(S,2));
S = logical(fliplr(S(idx,:)));
pU = arrayfun(@(i) U(S(i,:)), 2:2^n, 'UniformOutput', false);
[~,key] = sort(idx);
key = key(2:end)-1;
end

function [xi,wg,hi,gamma] = dist2quad(dist)
% map distributions to appropriate quadratures
qdeg = dist{end};
switch (lower(dist{1}))
    case 'normal'
        [xi,wg] = quadrature.quadrature('hermite',qdeg);
        hi = quadrature.quadpoly('hermite',qdeg-1,xi);
        mu = dist{2};
        sigma2 = dist{3};
        xi = mu + sqrt(sigma2)*xi;
        gamma = (hi.*hi)'*wg;
    case 'lognormal'
        [xi,wg] = quadrature.quadrature('hermite',qdeg);
        hi = quadrature.quadpoly('hermite',qdeg-1,xi);
        E = dist{2}; V = dist{3};
        mu = log(E^2/sqrt(E^2 + V));
        sigma2 = log(1+V/E^2);
        xi = exp(mu + sqrt(sigma2)*xi);
        gamma = (hi.*hi)'*wg;
    case 'uniform'
        [xi,wg] = quadrature.quadrature('legendre',qdeg);
        hi = quadrature.quadpoly('legendre',qdeg-1,xi);
        a = dist{2}; b = dist{3};
        xi = (b*(1+xi)+a*(1-xi))/2;
        gamma = (hi.*hi)'*wg;
    case 'exponential'
        [xi,wg] = quadrature.quadrature('laguerre',qdeg);
        hi = quadrature.quadpoly('laguerre',qdeg-1,xi);
        lambda = dist{2};
        xi = xi/lambda;
        gamma = (hi.*hi)'*wg;
    otherwise
        error('Distribution ''%s'' was not recognized',dist{1})
end
end

% =================== INPUT PARAMETERS ====================================
% =================== INPUT PARAMETERS ====================================
% =================== INPUT PARAMETERS ====================================

function optout= inputparse(optin)
optout.nMoments = 2;
optout.lvlSobol = 3; %'all';
err = false;
if numel(optin) == 1
    optin = optin{1};
    if isstruct(optin)
        fn = fieldnames(optin);
        for i = 1:numel(fn)
            switch (lower(fn{i}))
                case 'nmoments'
                    optout.nMoments = optin.(fn{i});
                case 'lvlsobol'
                    optout.lvlSobol = optin.(fn{i});
                otherwise
                    err = true;
            end
        end
    else
        err = true;
    end
elseif mod(numel(optin),2) == 0
    for i = 1:numel(optin)/2
        switch (lower(optin{2*i-1}))
            case 'nmoments'
                optout.nMoments = optin{2*i};
            case 'lvlsobol'
                optout.lvlSobol = optin{2*i};
            otherwise
                err = true;
        end
    end
else
   err = true;
end

if err
    error('The optional parameters must be passed  as either a struct or alternating parameter name and values');
end
end

function inputchk(modelfun, distributions)
% Size check
n = nargin(modelfun);
m = numel(distributions);
assert(n==m, 'There must be a distribution for each imput parameter');
% other checks ... ?
end