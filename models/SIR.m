%
% MIT License
% 
% Copyright (c) 2020 Bjørn Jensen
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
classdef SIR
    properties
        beta (:,:) double {mustBeNonnegative};  % Rate of a meeting leading to infection.
        gamma (1,:) double {mustBeNonnegative}; % Rate of recovery
        N (1,:) double {mustBeNonnegative};     % Population sizes
        names (1,:) cell;
    end
    properties (Dependent)
        tauRecovery;   % Recovery   (1./gamma)
        numGroups;
    end
    % =====================================================================
    % === METHODS =========================================================
    % =====================================================================
    methods
        function obj = SIR(beta,gamma,N,varargin)
            % Inputs 
            %   beta : <scalar>/<n-vector>/<nxn-matrix>
            %            Rate of a meeting leading to infection.
            %   gamma: <scalar>/<n-vector> : Rate of recovery
            %   N    : <scalar>/<n-vector> : Population sizes
            %
            % Examples
            %   SIR(beta,sigma,gamma,N)
            %
            % Notes:
            %   1) 'n' is the number of population groups
            %   2) A vector 'beta' is equivalent to matrix diag(beta)
            %      The population groups are then independent.
            %
            
            % Empty initialization
            if nargin == 0
                beta = 1;
                gamma = 1;
                N = 1;
            end
            
            % Check group number consistency
            n = length(N);
            check = cellfun(@(x) numel(x) == n | numel(x) == 1,{gamma});
            assert(all(check),'Number of groups must be congruent') 
            assert(numel(beta) == 1 || numel(beta) == n || all(size(beta) == [n,n]), ...
                '''beta'' must be a scalar, vector or square matrix.')
            
            if numel(beta) == 1,  beta  = beta *ones(1,n);  end
            if numel(gamma) == 1, gamma = gamma *ones(1,n); end
            if numel(beta) == n,  beta  = diag(beta);       end
            
            % Create object
            obj.beta = beta;
            obj.gamma = gamma;
            obj.N = N;
            
            % Optional parameters
            if ~isempty(varargin)
                parameter = varargin(1:2:end);
                parameter = arrayfun(@(x) {lower(x{1})}, parameter);
                value = varargin(2:2:end);

                % Group names
                [val,idx] = ismember('names',parameter);
                if val
                    names = value{idx};
                    assert(numel(names) == n,'Number of group names do not match number of groups')
                    obj.names = names; 
                end
                
            end
        end
        % -----------------------------------------------------------------
        % -----------------------------------------------------------------
        function [t,X] = solve(o,tspan,X0,~)
            % Runs ode45-solver for the input parameter
            %
            % Example
            %   SEIR.solve(tspan,X0)
            %   SEIR.solve(tspan,X0,'total')
            [t,X] = ode45(o.odefun,tspan,X0);
            
            
            % Sum across groups
            if nargin > 3
                n = o.num_groups;
                X = sum(reshape(X,[numel(X)/n,n]),2);
                X = reshape(X,[numel(X)/4,4]);
            end
        end

        function dxdt = odefun(o)
            % ODE function for solvers like ode45
            dxdt = @(t,x) o.ODEFUN(t,x,o.beta,o.gamma,o.N);
        end
        
        % -----------------------------------------------------------------
        % -----------------------------------------------------------------
        % Get-Set methods for Dependent properties
        function val = get.tauRecovery(o)
            val = 1./o.gamma;
        end
        function o = set.tauRecovery(o,val)
            o.gamma = 1./val;
        end
        
        function val = get.numGroups(obj)
            val = numel(obj.N);
        end
    end
    % =====================================================================
    % === METHODS (STATIC) ================================================
    % =====================================================================
    properties (Constant)
        NAME = 'SIR model';
    end
    
    methods (Static)
        function [params, desc, lims] = PARAMETERS
            % Lists model parameters
            
            % parameter name | descrition | freedom*
            % 
            % *if freedom is [] (empty) then the parameter must be set
            parameters = {
                'S0',    'initial condition S',  [0, inf];
                'I0',    'initial condition I',  [0, inf];
                'R0',    'initial condition R',  [0, inf];
                'beta',  'rate of infection',    [0, inf];
                'gamma', 'rate of recovery',     [0, inf];
                'N',     'population size',      [];
            };
            params = (parameters(:,1))';
            desc = (parameters(:,2))';
            lims = (parameters(:,3))';
        end
        
        function check = SIZECHECK(varargin)
            % takes in parameter size in the order from obj.PARAMETERS and
            % checks that they work together.
            szbeta = varargin{4};
            szN = varargin{6};
            
            chk1 = all(cellfun(@(x) all(x == szN) || all(x == [1,1]), ...
                                                    varargin([1,2,3,5])));
            chk2 = prod(szbeta) == 1 || all(szbeta == szN) || ...
                                               prod(szbeta) == max(szN)^2;
            check = chk1 && chk2;
        end
        
        function varargout = ENSURESIZE(varargin)
            % Transforms parameters from scalar to vectors and matrices so
            % they match for computation.
            [S0,I0,R0,beta,gamma,N] = varargin{:};
            n = numel(N);
            if numel(S0) == 1,  S0  = S0 *ones(1,n);  end
            if numel(I0) == 1,  I0  = I0 *ones(1,n);  end
            if numel(R0) == 1,  R0  = R0 *ones(1,n);  end
            if numel(beta) == 1,  beta  = beta *ones(1,n);  end
            if numel(gamma) == 1, gamma = gamma *ones(1,n); end
            if numel(beta) == n,  beta  = diag(beta);       end
            varargout = {S0,I0,R0,beta,gamma,N};
        end
        
        function [vars, desc] = OUTPUTS
            % Lists model outputs
            
            % output name | descrition
            outputs = {
                'S', 'Susceptible population';
                'I', 'Infectious population';
                'R', 'Recovered population';
            };
            vars = (outputs(:,1))';
            desc = (outputs(:,2))';
        end
        
        function dxdt = ODEFUN(t,x,beta,gamma,N)
            % ODE function for solvers like ode45
            
            % X = [ S(1), S(2), ..., S(n)
            %       I(1), I(2), ..., I(n)
            %       R(1), R(2), ..., R(n) ]
            % n = #(population groups)
            dXdt = @(t,X) [
               -((X(2,:)./N)*beta).*X(1,:);
                ((X(2,:)./N)*beta).*X(1,:) - gamma.*X(2,:);
                gamma.*X(2,:)
            ];
            
            % ode45 wants vector inputs
            mat = @(x) reshape(x,[3,numel(N)]);
            vec = @(x) x(:);
            
            dxdt = vec(dXdt(t,mat(x)));            
        end
        
        function [t,S,I,R] = RUNWITHPARAMS(tspan,S0,I0,R0,beta,gamma,N)
            % Runs SEIR model with specified parameters
            X0 = [S0(:),I0(:),R0(:)];
            [t,X] = ode45(@(t,x) SIR.ODEFUN(t,x,beta,gamma,N),tspan,X0(:));
            S = X(:,1:3:end);
            I = X(:,2:3:end);
            R = X(:,3:3:end);
        end  
    end
end
