% MCMC

clear all
close all
disp('  ')
% =========================================================================
% >>>   UQ EXAMPLE   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
% -------------------------------------------------------------------------
addpath('../testDesigner/')
addpath('../distributions/')
addpath('../models/')
addpath('../visualization/')
addpath('../tools/')

% --- Initialize ----------------------------------------------------------
N = 5.8e6;       % Population
tau_inf = 3.3;     % [days] ; infectious/recovery time
tau_inc = 4.2;     % [days] ; incubation time
R0 = 1.4;          % Replication number

% --- Parameters ----------------------------------------------------------
E = [R0, tau_inc, tau_inf];
V = [0.025^2, 0.25^2, 0.25^2];
sigma2 = log(1+V./E.^2);
sigma = sqrt(sigma2);
mu = log(E) - sigma2/2;

% --- ODE settings --------------------------------------------------------
X0 = [N-1,0,1,0];  % Initially 1 infected
m = 1000;          % Number of evaluation nodes
tspan = linspace(0, 24*30, m); 
                   % model time span

% --- MCMC ----------------------------------------------------------------
params = {mu,sigma,N,tspan,X0};
n = 1e4;
proposal_sigma = 0.1; % 0.05 and 0.01 are also interesting
m = 8;

samples = cell(1,m);
values = cell(1,m);
accept_rate = cell(1,m);
for i = 1:m
    rng(i-1) % for reproducability
    [samples{i}, values{i}, accept_rate{i}] = ...
        mcmc(n, proposal_sigma, params);
end
%%

% --- Plots ---------------------------------------------------------------
density_marginalized_plot = true;
cumulative_plot = true;

rng(24) % 24 seems like a good spread of colors
for i = 1:m
    if i == 1
        disp(' ')
        disp(['--: Accept rates : --','-'*ones(1,60)])
        for j = 1:m
            fprintf('| %d: %4.1f ', j, 100*accept_rate{j})
        end
        fprintf('|\n')
        disp(['', '-'*ones(1,81)])
        disp(['Average rate: ',num2str(100*mean([accept_rate{:}]))])
    end

    if density_marginalized_plot && i == 1
        xy = values{i};
        xy = xy';
        
        nbins = 50;
        [counts, bins] = hist3(xy,[nbins,nbins/2],'CDataMode','auto');
        %hist3(xy,[nbins,nbins],'CDataMode','auto')
        %colormap spring
        %colorbar
        %axis auto
        
        figure(2); clf;
        I_margin = sum(counts,1)'/sum(counts(:));
        t_margin = sum(counts,2)/sum(counts(:));
        t_bins = bins{1};
        I_bins = bins{2};
        t_h = t_bins(2)-t_bins(1);
        I_h = I_bins(2) - I_bins(1);

        subplot(211)
        bar(t_bins, t_margin/t_h, 'FaceColor', [0,.7,1])
        xlim([220, 350])
        ylim([0,0.025])
        grid minor
        xlabel('time [days since outbreak]')
        %t_mean = sum(t_margin .* t_bins');
        %t_var  = sum(t_margin .* (t_bins' - t_mean).^2);
        %I_mean = sum(I_margin .* I_bins');
        %I_var  = sum(I_margin .* (I_bins' - I_mean).^2);
        %disp(['t | mean: ',num2str(t_mean)])
        %disp(['t | std : ',num2str(sqrt(t_var))])
        %disp(['I | mean: ',num2str(I_mean)])
        %disp(['I | std : ',num2str(sqrt(I_var))])
        subplot(212)
        bar(I_bins, I_margin/I_h, 'FaceColor', [1,.7,0])
        xlim([0.85, 1.45]*10^5)
        grid minor
        xlabel('Infectious [count]')
        drawnow(); pause(0.1);

        figure(3); clf;
        h = pcolor(t_bins, I_bins, counts'/sum(counts(:)));
        set(h, 'EdgeColor', 'none');
        colormap(flipud(bone))
        xlim([210,390])
        ylim([0.75, 1.65]*10^5)
        box on
        colorbar
        xlabel('time of peak')
        ylabel('peak height')

        prefix = ['mcmc',num2str(i),'_'];
        exportgraphics(figure(2),[prefix 'marginal.png'],'Resolution',300)
        exportgraphics(figure(3),[prefix 'density.png'],'Resolution',300)
    end

    if cumulative_plot
        if i == 1 && ishandle(5), close(5), end
        figure(5);
        p = get(5,'Position');
        if i == 1
            set(5, 'Position', [p(1:2) 2*p(3),p(4)])
            clf
            EEE = zeros(m,1);
            SSS = zeros(m,1);
        end
        zzz = values{i};
        zz = cumsum(zzz,2);
        iter = 1:size(zzz,2);
        EE = zz./iter;
        VV = cumsum((zzz - EE).^2,2)./(iter-1);
        SS = sqrt(VV);

        %fprintf('mean: %6.2f\n', EE(1,end))
        %fprintf('std : %6.2f\n', SS(1,end))

        subplot(211); hold on
        if false && i == 1
            plot(iter([1,end]),mean(zzz(1,:))*ones(1,2),'--',...
                'Color',[0,0,0],...
                'LineWidth',2)
        end
        col = rand(1,3);
        plot(iter, EE(1,:), 'Color', col)
        if i == 1
            xlim([0,10000])
            ylim([270, 300])
            grid minor
            box on
            ylabel('cumulative mean')
        end

        subplot(212); hold on
        if false && i == 1
            plot(iter([1,end]),std(zzz(1,:))*ones(1,2),'--','Color',[.7,.7,.7])
        end
        plot(iter, SS(1,:),'Color', col)
        if i == 1
            xlim([0,10000])
            ylim([15,25])
            grid minor
            box on
            xlabel('iterations')
            ylabel('cumulative std')
        end

        if i == 1
            EEE = zeros(2,m);
            SSS = zeros(2,m);
        end
        EEE(:,i) = EE(:, end);
        SSS(:,i) = SS(:, end);
        if i == m
            disp(' ')
            disp(['--: t :--','-'*ones(1,26)])
            disp(['| Mean (±) : ', ...
                sprintf('% 9.2f (± %6.2f) |', mean(EEE(1,:)), std(EEE(1,:)))])
            disp(['|  Std (±) : ', ...
                sprintf('% 9.2f (± %6.2f) |', mean(SSS(1,:)), std(SSS(1,:)))])
            disp(['--: I :--','-'*ones(1,26)])
            disp(['| Mean (±) : ', ...
                sprintf('%9.2f (± %6.2f) | x10^3', mean(EEE(2,:))/1e3, std(EEE(2,:))/1e3)])
            disp(['|  Std (±) : ', ...
                sprintf('% 9.2f (± %6.2f) | x10^3', mean(SSS(2,:))/1e3, std(SSS(2,:))/1e3)])
            disp(['','-'*ones(1,35)])
        end

        if i == m
            prefix = ['mcmc_'];
            exportgraphics(figure(5),[prefix 'cumulative_mean_std.png'],'Resolution',300)
        end
    end
end


% =========================================================================
% MCMC FUNCTION

function [samples,values,accept_rate] = mcmc(n_iter, p_sig, params)
    mu = params{1};
    sigma = params{2};
    N = params{3};
    tspan = params{4};
    X0 = params{5};

    dists = {
        lognormal(mu(1),sigma(1)),
        lognormal(mu(2),sigma(2)),
        lognormal(mu(3),sigma(3))
    };

    fun = @(samp) modelfun(samp(1),samp(2),samp(3),N,tspan,X0);

    samples = zeros(3, n_iter);
    values = zeros(2, n_iter);
    samples(:,1) = exp(mu + sigma*randn(3,1));
    values(:,1) = fun(samples(:,1));
    accept = 0;
    for i = 1:n_iter
        if ~mod(i,1)
            fprintf('iter %04d | %6.2f pct\n',i,100*i/n_iter);
        end
        curr = samples(:,i);
        val_curr = values(:,i);

        p_curr = sample_prob(curr, dists);
        prop = curr + p_sig*randn(3,1);
        p_prop = sample_prob(prop, dists);
        alpha = p_prop / p_curr;
        if rand() < alpha
            accept = accept + 1;
            val_curr = fun(prop);
            curr = prop;
        end
        samples(:,i+1) = curr;
        values(:,i+1) = val_curr;
    end
    accept_rate = accept/n_iter;
end

% =========================================================================
% MODEL FUNCTION

function p = sample_prob(sample, dists)
    p = zeros(3,1);
    for i = 1:3
        p(i) = dists{i}.pdf(sample(i));
    end
    p = prod(p);
end

function out = modelfun(R0,tau_inc,tau_inf,N,tspan,X0)
    gamma = 1/tau_inf;
    sigma = 1/tau_inc;
    beta = R0*gamma;
    [t,X] = ode45(@(t,x) SEIR.ODEFUN(t,x,beta,sigma,gamma,N),tspan,X0);
    I = X(:,3);
    [Ipeak, idx] = max(I);
    tpeak = t(idx);
    out = [tpeak; Ipeak];
end