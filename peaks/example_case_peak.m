%
% SCRIPT - PEAK TIME CASE EXAMPLE 
%
%   This script runs the peak time case from [1] to compute an estimate for
%   the peak timing and magnitude of infectious individuals in an evolving 
%   epidemic.
%
% Code written by Bjørn Jensen for the COVID-19-CTRL joint project between
% The Technical University of Denmark (DTU) and Aalborg University (AAU).
%
% Copyright (c) 2020 Bjørn Jensen 
%
% [1] Efficient Uncertainty Quantification of  Predictions using Polynomial 
%     Chaos in Epidemic Modeling (2021) - Jensen B., Engsig-Karup A.P., 
%     Knudsen K.
%
clear all
close all
disp('  ')
% =========================================================================
% >>>   UQ EXAMPLE   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
% -------------------------------------------------------------------------
addpath('../testDesigner/')
addpath('../distributions/')
addpath('../models/')
addpath('../visualization/')
addpath('../tools/')

% --- Initialize ----------------------------------------------------------
N = 5.8e6;       % Population
tau_inf = 3.3;     % [days] ; infectious/recovery time
tau_inc = 4.2;     % [days] ; incubation time
R0 = 1.4;          % Replication number

% --- Parameters ----------------------------------------------------------
quad_deg = 3;      % use 3 or 10 for the figures from the manuscript

dist = {           % Distributiuons for the parameters
    {'lognormal', R0, 0.025^2, quad_deg} ...
    {'lognormal', tau_inc, 0.25^2, quad_deg} ...
    {'lognormal', tau_inf, 0.25^2, quad_deg}
};

% --- ODE settings --------------------------------------------------------
X0 = [N-1,0,1,0];  % Initially 1 infected
m = 1000;          % Number of evaluation nodes
tspan = linspace(0, 24*30, m); 
                   % model time span

% --- TESTDESIGN ----------------------------------------------------------
tic;
[moments,sobol,coefX,gamma,X] = testdesigner(...
    @(r0,tinc,trec) modelfun(r0,tinc,trec,N,tspan,X0), ...
    dist, 'lvlSobol', 'all');
toc;
mu = moments{1};
sig2 = moments{2};
X = reshape(X, [2, quad_deg^length(dist)])';

disp(' ')
fprintf('--: Quadrature order % 3d :---\n', quad_deg)
fprintf('|  t  |   Mean  :  % 7.2f  |\n', mu(1))
fprintf('|     |    Std  :  % 7.2f  |\n', sqrt(sig2(1)))
fprintf('-----------------------------\n')
fprintf('|  I  |   Mean  :  % 7.2f  | x10^3\n', mu(2)/1e3)
fprintf('|     |    Std  :  % 7.2f  | x10^3\n', sqrt(sig2(2))/1e3)
fprintf('-----------------------------\n')
disp(' ')

% --- VISUALIZE -----------------------------------------------------------

% ====================================================
% ====================================================
figure(1)
set(1,'Position',get(1,'Position').*[1,1,1.5,1.5])

% Get probability weights
[xi,wg] = quadrature.quadrature('hermite',quad_deg);
tt = nd_outer_prod(wg,wg,wg);
tt = tt(:)./max(tt(:));

% Scatter plot
p = scatter(X(:,1),X(:,2),20+200*tt,'o',...
    'MarkerEdgeColor', [0,0,0], ...
    'MarkerFaceColor', 'flat', ...
    'MarkerFaceAlpha', .75);
grid minor
colorbar
colormap spring
cm = colormap('Spring');
set(p,'CData',cm(round(255*tt)+1,:))

% Labels
xlabel('time of peak')
ylabel('peak height')
title('Sampling points')
set(gca,'FontSize',14)
xx = xlim;
if xx(2) > 700
    xlim([0,700])
end
legend("Model evaluations, size and color by sample relative distribution weight")

% ====================================================
% ====================================================
figure(2); clf;
set(2,'Position',get(2,'Position').*[1,1,1.5,1.5])

% ----------------------------------------------------
subplot(2,1,1) 
hold on

col = [0,.7,1]; % Color light blue
X = lognormal(mu(1), sig2(1), 'SetByMeanVar');

% Curve
plot(tspan, X.pdf(tspan), '-', 'Color', [.7,.7,.7])

% Confidence interval .95
cfint = X.confidence_interval(.95);
tt = tspan(cfint(1) <= tspan & tspan <= cfint(2));
qq(1) = areaplot(tt, X.pdf(tt), 'Color', col + (1-0.35)*([1,1,1]-col));

% Confidence interval .75
cfint = X.confidence_interval(.75);
tt = tspan(cfint(1) <= tspan & tspan <= cfint(2));
qq(2) = areaplot(tt, X.pdf(tt), 'Color', col + (1-0.35)^2*([1,1,1]-col));

% Labels and such
grid minor
xlim([220, 350])
title('Position of peak infectious')
xlabel('time [days since outbreak]')
set(gca,'FontSize',14)
legend(qq,'95% conf. int.','75% conf. int.')

% ----------------------------------------------------
subplot(2,1,2) 
hold on

col = [1,.7,0]; % Color orange
X = lognormal(mu(2), sig2(2), 'SetByMeanVar');

% Curve
cfint = X.confidence_interval(.999);
xx = linspace(cfint(1), cfint(2), 200);
plot(xx, X.pdf(xx), '-', 'Color', [.7,.7,.7])

% Confidence interval .95
cfint = X.confidence_interval(.95);
tt = xx(cfint(1) <= xx & xx <= cfint(2));
qq(1) = areaplot(tt, X.pdf(tt), 'Color', col + (1-0.35)*([1,1,1]-col));

% Confidence interval .75
cfint = X.confidence_interval(.75);
tt = xx(cfint(1) <= xx & xx <= cfint(2));
qq(2) = areaplot(tt, X.pdf(tt), 'Color', col + (1-0.35)^2*([1,1,1]-col));

% Labels and such
xlim([0.85e5, 1.45e5])
%set(gca,'XTick',[1.05,1.1,1.15,1.2]*1e5)
title('Peak infectious modulus')
xlabel('Infectious [count]')
set(gca,'FontSize',14)
grid minor
legend(qq,'95% conf. int.','75% conf. int.')

% ====================================================
% ====================================================
figure(3); clf;
set(3,'Position',get(3,'Position').*[1,1,1.5,1.5])

% ----------------------------------------------------
subplot(2,1,1)

col = [0,.7,1]; % Color light blue

% Bar plot
h = bar(sobol(1,:),'FaceColor',col);

% Add text on top of the bars
for i = 1:size(sobol,2)
    text(i, sobol(1,i), sprintf('%4.3f', sobol(1,i)), ...
        'HorizontalAlignment', 'center', ...
        'VerticalAlignment', 'bottom', ...
        'FontSize', 13)
end

% Labels and such
set(gca,'XTickLabel', {'R_0','\tau_{inc}','\tau_{inf}','R_0,\tau_{inc}','R_0,\tau_{inf}','\tau_{inc},\tau_{inf}','all 3'})
set(gca,'FontSize',14)
ylim([0,1])
xlim([0.5,7.5])
grid minor
title('Sobol indices for peak time')
ylabel('contributions')

% ----------------------------------------------------
subplot(2,1,2)

col = [1,.7,0]; % Color orange

% Bar plot
h = bar(sobol(2,:),'FaceColor',col);

% Add text on top of the bars
for i = 1:size(sobol,2)
    text(i, sobol(2,i), sprintf('%4.3f', sobol(2,i)), ...
        'HorizontalAlignment', 'center', ...
        'VerticalAlignment', 'bottom', ...
        'FontSize', 13)
end

% Labels and such
set(gca,'XTickLabel',{'R_0','\tau_{inc}','\tau_{inf}','R_0,\tau_{inc}','R_0,\tau_{inf}','\tau_{inc},\tau_{inf}','all 3'})
set(gca,'FontSize',14)
ylim([0,1])
xlim([0.5,7.5])
grid minor
title('Sobol indices for peak modulus')
ylabel('contributions')

% ========================================================
% Save figures
prefix = 'A_';
if quad_deg == 10
    prefix = 'B_';
end
exportgraphics(figure(1),[prefix 'samps.png'],'Resolution',300)
exportgraphics(figure(2),[prefix 'dist.png'],'Resolution',300)
exportgraphics(figure(3),[prefix 'sobol.png'],'Resolution',300)

% =========================================================================
% MODEL FUNCTION

function out = modelfun(R0,tau_inc,tau_inf,N,tspan,X0)
    gamma = 1/tau_inf;
    sigma = 1/tau_inc;
    beta = R0*gamma;
    [t,X] = ode45(@(t,x) SEIR.ODEFUN(t,x,beta,sigma,gamma,N),tspan,X0);
    I = X(:,3);
    [Ipeak, idx] = max(I);
    tpeak = t(idx);
    out = [tpeak; Ipeak];
end
