%
% SCRIPT - SUPERSPREADER CASE EXAMPLE - SIMULATION WITH UQ
%
%   This script runs the superspreader case from [1]. It runs a simulation
%   based on the numbers computed with the fitting script. The plots from
%   [1] are generated using this.
%
% Code written by Bjørn Jensen for the COVID-19-CTRL joint project between
% The Technical University of Denmark (DTU) and Aalborg University (AAU).
%
% Copyright (c) 2020 Bjørn Jensen 
%
% [1] Efficient Uncertainty Quantification of  Predictions using Polynomial 
%     Chaos in Epidemic Modeling (2021) - Jensen B., Engsig-Karup A.P., 
%     Knudsen K.
%

close all
clear all
clc
% ========================================================================
% ========================================================================
addpath('../testDesigner/')
addpath('../distributions/')
addpath('../visualization/')
addpath('../tools/')
% ------------------------------------------------------------------------
t = 0:180;
c1 = 0.130301;
c2 = 0.186898;
c3 = 0.188040;
restriction = {
    [1,c1,c2,c3]
    [16,46,86]
};
qdeg = 6;
dist = {
    {'normal', c1, (0.10*c1)^2, qdeg}
    {'normal', c2, (0.10*c2)^2, qdeg}
    {'normal', c3, (0.10*c3)^2, qdeg}
};

[moments,sobol,coef,gamma] = testdesigner(@(c1,c2,c3) testdesignfun(t,c1,c2,c3), dist,'nmoments',3);
mu = moments{1};
sig2 = moments{2};
mu = reshape(mu,[numel(t),numel(mu)/numel(t)]);
sig2 = reshape(sig2,[numel(t),numel(sig2)/numel(t)]);
X = mu;
% ------------------------------------------------------------------------
figure(5); clf;

tl = tiledlayout(5,1);
tl.TileSpacing = 'compact';
tl.Padding = 'compact';

nexttile([1,1]);
plot(t,X(:,1),'LineWidth',2);
legend('S');

nexttile([1,1]); hold on
T = t(1):t(end);
total_infected = interp1(t,X(:,10),T);
plot(T,total_infected,'LineWidth',2);
plot(t,X(:,8),'LineWidth',2);
legend('Total Infected','R');

nexttile([2,1]);
plot(t,X(:,[2,3,4,5]),'LineWidth',2);
legend('E','I1','I2','W');
ylim([0,2e4])
nexttile([1,1])
plot_restriction(t,restriction)

saveas(gcf,'img1.png')
% ------------------------------------------------------------------------
%figure(6); clf;
%tl = tiledlayout(5,1);
%tl.TileSpacing = 'compact';
%tl.Padding = 'compact';

%nexttile([4,1]); hold on
figure(31); clf; hold on
p = get(gcf,'Position');
set(gcf,'Position',[p(1) p(2) p(3) .7*p(4) ])

tn6 = truncnormal(mu(:,6), sig2(:,6), 0, 5.8e6,'SetByMeanVar');
tn7 = truncnormal(mu(:,7), sig2(:,7), 0, 5.8e6,'SetByMeanVar');
%tn6 = lognormal(mu(:,6), sig2(:,6), 'SetByMeanVar');
%tn7 = lognormal(mu(:,7), sig2(:,7), 'SetByMeanVar');
sig2(sig2(:,6)==0,6) = eps;
sig2(sig2(:,7)==0,7) = eps;

z = tn6.confidence_interval(.95);
areaplot(T(2:end),z(2:end,:),'Color',[     0    0.4470    0.7410],'FaceAlpha',.25,'EdgeColor',[     0    0.4470    0.7410],'LineStyle',':');
z = tn6.confidence_interval(.50);
%areaplot(T(2:end),z(2:end,:),'Color',[1,0,0],'FaceAlpha',.25);
z = tn7.confidence_interval(.95);
areaplot(T(2:end),z(2:end,:),'Color',[0.8500    0.3250    0.0980],'FaceAlpha',.25,'EdgeColor',[0.8500    0.3250    0.0980],'LineStyle',':');
z = tn7.confidence_interval(.50);
%areaplot(T(2:end),z(2:end,:),'Color',[0,0,1],'FaceAlpha',.25);

%areaplot(T,tn6.confidence_interval(.95),[1,0,0],'FaceAlpha',.25);
%areaplot(T,tn6.confidence_interval(.50),[1,0,0],'FaceAlpha',.25);
%areaplot(T,tn7.confidence_interval(.95),[0,0,1],'FaceAlpha',.25);
%areaplot(T,tn7.confidence_interval(.50),[0,0,1],'FaceAlpha',.25);
h1 = plot(t,X(:,6),'Color',[     0    0.4470    0.7410]);
h2 = plot(t,X(:,7),'Color',[0.8500    0.3250    0.0980]);

[Tn,D] = newly_hosp_data;
Tn = datenum(Tn);
Tn = Tn - Tn(1);
h3 = plot(Tn,D,'.','MarkerSize',20,'Color',[0.9290    0.6940    0.1250]);

%T = t(1):t(end);
%total_admissions = interp1(t,X(:,end),T);
%new_admissions_by_day = diff(total_admissions);
%plot(T(2:end),new_admissions_by_day,':','LineWidth',3);

%plot(T,X(:,end),':','LineWidth',3);
%plot(T,X(:,end)+sqrt(sig2(:,end)),':','LineWidth',3);
%plot(T,X(:,end)-sqrt(sig2(:,end)),':','LineWidth',3);

%tn = truncnormal(mu(:,end), sig2(:,end), 0, 5.8e6,'SetByMeanVar');

tn = lognormal(mu(:,end), sig2(:,end), 'SetByMeanVar');
z = tn.confidence_interval(.95);
areaplot(T(2:end),z(2:end,:),'Color',[.7,0,.7],'FaceAlpha',.25,'EdgeColor',[1,0,1],'LineStyle',':');
z = tn.confidence_interval(.50);
%areaplot(T(2:end),z(2:end,:),'Color',[.7,0,.7],'FaceAlpha',.25);
%areaplot(T,tn.confidence_interval(.95),[.7,0,.7],'FaceAlpha',.25);
%areaplot(T,tn.confidence_interval(.50),[.7,0,.7],'FaceAlpha',.25);
h4 = plot(T,mean(tn),':','Color',[.7,0,1],'LineWidth',3);
%h4 = plot(T,mu(:,end),':','Color',[1,0,.7],'LineWidth',3);

%total_admissions = interp1(t,X(:,end)+sqrt(sig2(:,end)),T);
%new_admissions_by_day = diff(total_admissions);
%plot(T(2:end),new_admissions_by_day,':','LineWidth',3);

ylabel('individuals')
legend([h1,h2,h3,h4],'H','C','Newly Admitted (data)','Newly Admitted (sim)');
xlabel('days')
grid
box on

saveas(gcf,['img',num2str(get(gcf,'Number')),'.png'])
% ------------------------------------------------------------------------
figure(7); clf;
tl = tiledlayout(5,1);
tl.TileSpacing = 'compact';
tl.Padding = 'compact';

nexttile([4,1])

T = t(1):t(end);
total_infected = interp1(t,X(:,10),T);
stem(T,total_infected);
legend('Total infected')

nexttile([1,1])
plot_restriction(t,restriction)

saveas(gcf,'img3.png')
% ------------------------------------------------------------------------
figure(8); clf;
tl = tiledlayout(5,1);
tl.TileSpacing = 'compact';
tl.Padding = 'compact';

nexttile([4,1]); hold on;

T = t(1):t(end);
total_infected = interp1(t,X(:,10),T);
growth_pct = [0,diff(total_infected)]./total_infected;
stem(T,growth_pct);
plot(t([1,end]),mean(growth_pct(13:19))*[1,1],'--')
text(t(end),mean(growth_pct(13:19)),num2str(mean(growth_pct(13:19))),...
    'VerticalAlign','top','HorizontalAlign','right');
ylims = ylim;
plot([17,17],ylims,':')
legend('Growth')

nexttile([1,1])
plot_restriction(t,restriction)

% ------------------------------------------------------------------------
figure(32); clf; hold on
set(gcf,'Position',[p(1) p(2) p(3) .7*p(4) ])

sobol = reshape(sobol,[numel(t),11,numel(sobol)/(numel(t)*11)]);
S = cumsum(sobol,3);

% Get rid of the first time step as it is the initial condition and thus
% contains no stochasticity.
p1 = areaplot(T(16:end),squeeze(S(16:end,6,[1])),'Color',[1,.5,.5]);
p2 = areaplot(T(16:end),squeeze(S(16:end,6,[1,2])),'Color',[.5,1,.5]);
p3 = areaplot(T(16:end),squeeze(S(16:end,6,[2,3])),'Color',[.5,.5,1]);
p4 = areaplot(T(16:end),squeeze(S(16:end,6,[3,4])),'Color',[1,1,.5]);
p5 = areaplot(T(16:end),squeeze(S(16:end,6,[4,5])),'Color',[1,.5,1]);
p6 = areaplot(T(16:end),squeeze(S(16:end,6,[5,6])),'Color',[.5,1,1]);
ylim([0,1])
xlim(T([1,end]))
legend([p1,p2,p3,p4,p5,p6],'c_1','c_2','c_3','c_1c_2','c_1c_3','c_2c_3','location','south west')
ylabel('contributions')
box on
xlabel('days')

saveas(gcf,['img',num2str(get(gcf,'Number')),'.png'])

for i = 1:30
    if ishandle(i)
        set(i,'Visible','Off')
    end
end
% ========================================================================
% ========================================================================
function X = testdesignfun(T,c1,c2,c3)
ss = 0.8;
ss_p = 0.1;
beta_sc = 0.601642;
restriction = {
    [1,c1,c2,c3],
    [16,46,86]
};
I0 = 473.571950;

betafun = beta_time(ss,ss_p,beta_sc,restriction);

[beta,wg] = betafun(0);
disp('Mean R0')
disp(4.2*sum(beta.*wg))

tmp = beta.*wg;
disp('Superspreader contribution (%)')
disp(tmp(1)/sum(tmp))

disp('beta')
disp(beta)
for i = 1:numel(restriction{2})
    [beta,wg] = betafun(restriction{2}(i)+1);
    disp(4.2*sum(beta.*wg))
    disp(beta)
end
% ------------------------------------------------------------------------
X = run_model(T,betafun,I0);

t = T(1):T(end);
total_admissions = interp1(T,X(:,9),t);
new_admissions_by_day = [0,diff(total_admissions)];
X = [X(:);new_admissions_by_day(:)];
end


function [T,data] = newly_hosp_data

% WEBSITE DOESN'T EXIST ANYMORE!
% s = webread('https://api.covid19data.dk/ssi_newly_hospitalized');
% 
% T = {s.date}';
% T = datetime(T,'InputFormat','yyyy-MM-dd''T''hh:mm:''00+00:00''');
% data = [s.newly_hospitalized]';

tbl = readtable('Newly_admitted_over_time.csv');
T = tbl{:,1};
data = tbl{:,end};

end

% ------------------------------------------------------------------------
function plot_restriction(t,restriction)
F = pwfun(restriction{:});
plot(t,F(t),'r:','LineWidth',2);
for i = 1:length(restriction{2})
    text(restriction{2}(i),restriction{1}(1+i),[' ',num2str(restriction{1}(1+i))],...
        'VerticalAlign','bottom','HorizontalAlign','left');
end
ylim([0,1]);
end

% ------------------------------------------------------------------------
function b = beta_time(ss,p,sc_beta,restriction)
if nargin < 4, restriction = []; F = []; end
if ~isempty(restriction), F = pwfun(restriction{:}); end
ss_beta = ((1-p)/(1-ss))*(ss/p);
w = [p,1-p];
beta = ones(1, 2);
beta(1) = ss_beta;
mbeta = sum(beta.*w);
b = @betafun;
    function [b,wg] = betafun(t)
        sc = 1;
        if ~isempty(F), sc = F(t); end
        b = ones(1, 2);
        b(1) = ss_beta*sc;
        b(2) = min(1,ss_beta*sc);
        b = b*(sc_beta/mbeta);
        wg = w;
    end
end

% ------------------------------------------------------------------------
function f = pwfun(vals,jmppts)
    f = @fun;
    function y = fun(x)
        y = zeros(size(x));
        prejmp = -Inf;
        for i = 1:length(jmppts)
            jmp = jmppts(i);
            idx = x <= jmp & x > prejmp;
            y(idx) = vals(i);
            prejmp = jmp;
        end
        y(x > prejmp) = vals(end);
    end
end

% ------------------------------------------------------------------------
function X = run_model(T,betafun,I0)

% pde initial conditions and time span
N = 5.8e6;
X0 = [N-I0; I0/2; I0/3; I0/6; 0; 0; 0; 0; 0; I0];

% age group dynamics for hospital [10y groups 0-79 and 80+]
age_spread = [0.109, 0.119, 0.133, 0.117, 0.136, 0.136, 0.117, 0.089, 0.044];
prob_hosp = [0.00001, 0.00013, 0.0037, 0.011, 0.014, 0.027, 0.039, 0.055, 0.055];
prob_icu = [0.05, 0.05, 0.05, 0.05, 0.063, 0.122, 0.274, 0.432, 0.709];

% parameters
tau_incub = 1.2;
tau_presymp_inf = 1.2;
tau_inf = 3;
tau_wait = 2;
tau_hosp = 5;
tau_icu = 12;
sigma = 1/tau_incub;
gamma1 = 1/tau_presymp_inf;
gamma2 = 1/tau_inf;
gamma3 = 1/tau_wait;
alpha = 1/tau_hosp;
zeta = 1/tau_icu;
% Z1 :: Fraction of infected population getting hospitalized. This
%       calculation assumes that the age distribution of infected people is
%       close to the age distribution in society;
% Z2 :: Fraction of hospitalized population going into ICU. This uses the
%       derived age distribution for the hospitalized population: 
%          age_spread .* prob_hosp / z1
z1 = sum(age_spread.*prob_hosp); 
z2 = sum(age_spread.*prob_hosp.*prob_icu)/z1;

opts = {sigma,gamma1,gamma2,gamma3,z1,z2,alpha,zeta};

[t,X] = ode45(@model, T, X0, [], betafun, opts{:}, N);

end

% ------------------------------------------------------------------------
function dX = model(t,X,betafun,sigma,gamma1,gamma2,gamma3,z1,z2,alpha,zeta,N)
% SEIIWRHC-superspreaders
% 1:   dS(t) = -Int( beta(a) da=0..1)*(I1(t) + I2(t))*S(t)/N
% 2:   dE(t) = Int( beta(a) da=0..1)*(I1(t) + I2(t))*S(t)/N - sigma*E(t)
% 3:  dI1(t) = sigma*E(t) - gamma1*I1(t)
% 4:  dI2(t) = gamma1*I1(t) - gamma2*I2(t)
% 5:   dW(t) = gamma2*I2(t) - gamma3*W(t);
% 6:   dH(t) = z1*gamma3*W(t) - alpha*H(t)
% 7:   dC(t) = z2*alpha*H(t) - zeta*C(t)
% 8:   dR(t) = (1-z1)*gamma3*W(t) + (1-z2)*alpha*H(t) + zeta*C(t)
% Extra measurement compartments
% 10:  dNewlAmitted = z1*gamma3*W(t);
% 11:  dTotalInfected = Int( beta(a) da=0..1)*(I1(t) + I2(t))*S(t)/N
% 
[beta,wg] = betafun(t);
beta = beta.*wg;
X = num2cell(X);
[S,E,I1,I2,W,H,C,~] = X{:};
dX = [
    -sum(beta)*(I1+I2)*S/N;
    sum(beta)*(I1+I2)*S/N - sigma*E;
    sigma*E - gamma1*I1;
    gamma1*I1 - gamma2*I2;
    gamma2*I2 - gamma3*W;
    z1*gamma3*W - alpha*H;
    z2*alpha*H - zeta*C;
    (1-z1)*gamma3*W + (1-z2)*alpha*H + zeta*C;
    z1*gamma3*W;
    sum(beta)*(I1+I2)*S/N;
];

end