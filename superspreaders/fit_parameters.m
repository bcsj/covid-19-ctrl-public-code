%
% SCRIPT - SUPERSPREADER CASE EXAMPLE - FITTING 
%
%   This script runs the superspreader case from [1] to compute the fitted
%   parameters >>beta_sc<<, the beta-scaling, denoted by >>s<< in [1];
%   >>I0<<, the initial condition for the epidemic; as well as >>c(k)<< for
%   k=1,2,3, the restriction scales.
%
% Code written by Bjørn Jensen for the COVID-19-CTRL joint project between
% The Technical University of Denmark (DTU) and Aalborg University (AAU).
%
% Copyright (c) 2020 Bjørn Jensen 
%
% [1] Efficient Uncertainty Quantification of  Predictions using Polynomial 
%     Chaos in Epidemic Modeling (2021) - Jensen B., Engsig-Karup A.P., 
%     Knudsen K.
%

close all
clear all
clc
% ========================================================================
% ========================================================================
addpath('../tools')
% ------------------------------------------------------------------------

% ///////////////////////////////////////////////7
% /// GUESS ///
restriction = {
    [1, 0.125, 0.20, 0.25, 0.5]
    [15, 46, 86, 150]
};
ss_p = 0.1;
ss = 0.8;
igp = 0.23;
sc_beta = 0.6;
I0 = 600;
% ///////////////////////////////////////////////7

% /// GET DATA
[T,D] = newly_hosp_data;
T = datenum(T);
T = T - T(1);

options = optimoptions('fmincon', ...
    'Algorithm','interior-point', ...
    'display','iter', ...
    'MaxFunctionEvaluations',3e3, ...
    'MaxIterations',1e3);
% /// #1: FIT {sc_beta} and {I0}
initVals = [0.60, 600];
lb = [0.1,50]; ub = [1.5,1000];
fun1 = @(p) fitfun1(p(1),p(2),ss,ss_p,igp,T(1:15),D(1:15),0.01);
estParam = fmincon(fun1, ...
    initVals,[],[],[],[],lb,ub, [],options);

fit_beta_sc = estParam(1);
fit_I0 = estParam(2);

% /// #2: FIT {restriction}
initVals = [0.15,   0.20,   0.25];
lb = [0,0,0]; ub = [1,1,1];
fun2 = @(p) fitfun2(p,fit_beta_sc,fit_I0,ss,ss_p,T,D);
fun2db = @(p) fitfun2(p,fit_beta_sc,fit_I0,ss,ss_p,T,D,true);
estParam = fmincon(fun2, ...
    initVals,[],[],[],[],lb,ub, [],options);

disp('beta_sc & I0:')
fprintf('   %f     %f\n',fit_beta_sc,fit_I0);

disp('restriction:')
fprintf('   %f, %f, %f\n',estParam);

% ------------------------------------------------------------------------
function v = fitfun1(sc,i0,ss,p,igp_aim,Tnhd,Dnhd,alpha)
Tnhd = Tnhd(:);
Dnhd = Dnhd(:);

[~,igp,Tnad,Dnad] = datafun([],ss,p,sc,i0);
Tnad = Tnad(:);
Dnad = Dnad(:);

[~,idx1,idx2] = intersect(Tnad,Tnhd);

v = (1/2)*(igp-igp_aim)^2/igp_aim^2 + ...
    (alpha/2)*norm(Dnad(idx1) - Dnhd(idx2),2)^2/norm(Dnhd(idx2),2)^2;

end

% ------------------------------------------------------------------------
function v = fitfun2(rest,beta_sc,I0,ss,p,Tnhd,Dnhd)
Tnhd = Tnhd(:);
Dnhd = Dnhd(:);

restriction = {
    [1, rest]
    [16, 46, 86]
};

[~,~,Tnad,Dnad] = datafun(restriction,ss,p,beta_sc,I0);
Tnad = Tnad(:);
Dnad = Dnad(:);
[~,idx1,idx2] = intersect(Tnad,Tnhd);

v = (1/2)*norm(Dnad(idx1) - Dnhd(idx2),2)^2/norm(Dnhd(idx2),2)^2 ...
    -1000*(min(0,rest(2)-rest(1)) + min(0,rest(3)-rest(2)));

end

% ------------------------------------------------------------------------
function [R0,igp,T,nad] = datafun(restriction, ss, p, sc_beta, I0)

betafun = beta_time(ss, p, sc_beta, restriction);

[beta,wg] = betafun(0);
R0 = 4.2*sum(beta.*wg); 

tmp = beta.*wg;
ss = tmp(1)/sum(tmp); % superspreader contrib

[t,X] = run_model(betafun,I0);

T = t(1):t(end);
total_admissions = interp1(t,X(:,9),T);
nad = diff(total_admissions); % new_admissions_by_day

igp = [];
if isempty(restriction)
    total_infected = interp1(t,X(:,10),T);
    growth_pct = [0,diff(total_infected)]./total_infected;
    [~,idx] = min(diff(movingavg(growth_pct(2:end),10)));
    idx = floor((idx + 1)/2);
    igp = mean(growth_pct(idx-3:idx+3)); %init_growth_pct
end

end

% ------------------------------------------------------------------------
function [T,data] = newly_hosp_data

% WEBSITE DOESN'T EXIST ANYMORE!
% s = webread('https://api.covid19data.dk/ssi_newly_hospitalized');
% 
% T = {s.date}';
% T = datetime(T,'InputFormat','yyyy-MM-dd''T''hh:mm:''00+00:00''');
% data = [s.newly_hospitalized]';

tbl = readtable('Newly_admitted_over_time.csv');
T = tbl{:,1};
data = tbl{:,end};

end

function plot_restriction(t,restriction)
F = pwfun(restriction{:});
plot(t,F(t),'r:','LineWidth',2);
for i = 1:length(restriction{2})
    text(restriction{2}(i),restriction{1}(1+i),[' ',num2str(restriction{1}(1+i))],...
        'VerticalAlign','bottom','HorizontalAlign','left');
end
ylim([0,1]);
end

% ------------------------------------------------------------------------
function b = beta_time(ss,p,sc_beta,restriction)
% ss - assumed superspreader contribution
% p - percentage of population superspreading
% sc_beta - scaling to set average beta
% restriction - restriction function
if nargin < 4 || isempty(restriction), restriction = []; F = []; end
if ~isempty(restriction), F = pwfun(restriction{:}); end
ss_beta = ((1-p)/(1-ss))*(ss/p);
w = [p,1-p];
beta = ones(1, 2);
beta(1) = ss_beta;
mbeta = sum(beta.*w);
b = @betafun;
    function [b,wg] = betafun(t)
        sc = 1;
        if ~isempty(F), sc = F(t); end
        b = ones(1, 2);
        b(1) = ss_beta*sc;
        b(2) = min(1,ss_beta*sc);
        b = b*(sc_beta/mbeta);
        wg = w;
    end
end

% ------------------------------------------------------------------------
function f = pwfun(vals,jmppts)
    f = @fun;
    function y = fun(x)
        y = zeros(size(x));
        prejmp = -Inf;
        for i = 1:length(jmppts)
            jmp = jmppts(i);
            idx = x <= jmp & x > prejmp;
            y(idx) = vals(i);
            prejmp = jmp;
        end
        y(x > prejmp) = vals(end);
    end
end

% ------------------------------------------------------------------------
function [t,X] = run_model(betafun, I0)

% pde initial conditions and time span
N = 5.8e6;
X0 = [N-I0; I0/2; I0/3; I0/6; 0; 0; 0; 0; 0; I0];

tspan = [0, 180];

% age group dynamics for hospital [10y groups 0-79 and 80+]
age_spread = [0.109, 0.119, 0.133, 0.117, 0.136, 0.136, 0.117, 0.089, 0.044];
prob_hosp = [0.00001, 0.00013, 0.0037, 0.011, 0.014, 0.027, 0.039, 0.055, 0.055];
prob_icu = [0.05, 0.05, 0.05, 0.05, 0.063, 0.122, 0.274, 0.432, 0.709];

% parameters
tau_incub = 1.2;
tau_presymp_inf = 1.2;
tau_inf = 3;
tau_wait = 2;
tau_hosp = 5;
tau_icu = 12;
sigma = 1/tau_incub;
gamma1 = 1/tau_presymp_inf;
gamma2 = 1/tau_inf;
gamma3 = 1/tau_wait;
alpha = 1/tau_hosp;
zeta = 1/tau_icu;
% Z1 :: Fraction of infected population getting hospitalized. This
%       calculation assumes that the age distribution of infected people is
%       close to the age distribution in society;
% Z2 :: Fraction of hospitalized population going into ICU. This uses the
%       derived age distribution for the hospitalized population: 
%          age_spread .* prob_hosp / z1
z1 = sum(age_spread.*prob_hosp); 
z2 = sum(age_spread.*prob_hosp.*prob_icu)/z1;

opts = {sigma,gamma1,gamma2,gamma3,z1,z2,alpha,zeta};

[t,X] = ode45(@model, tspan, X0, [], betafun, opts{:}, N);

end

% ------------------------------------------------------------------------
function dX = model(t,X,betafun,sigma,gamma1,gamma2,gamma3,z1,z2,alpha,zeta,N)
% SEIIWRHC-superspreaders
% 1:   dS(t) = -Int( beta(a) da=0..1)*(I1(t) + I2(t))*S(t)/N
% 2:   dE(t) = Int( beta(a) da=0..1)*(I1(t) + I2(t))*S(t)/N - sigma*E(t)
% 3:  dI1(t) = sigma*E(t) - gamma1*I1(t)
% 4:  dI2(t) = gamma1*I1(t) - gamma2*I2(t)
% 5:   dW(t) = gamma2*I2(t) - gamma3*W(t);
% 6:   dH(t) = z1*gamma3*W(t) - alpha*H(t)
% 7:   dC(t) = z2*alpha*H(t) - zeta*C(t)
% 8:   dR(t) = (1-z1)*gamma3*W(t) + (1-z2)*alpha*H(t) + zeta*C(t)
% Extra measurement compartments
% 10:  dNewlAmitted = z1*gamma3*W(t);
% 11:  dTotalInfected = Int( beta(a) da=0..1)*(I1(t) + I2(t))*S(t)/N
% 
[beta, wg] = betafun(t);
beta = beta.*wg;
X = num2cell(X);
[S,E,I1,I2,W,H,C,~] = X{:};
dX = [
    -sum(beta)*(I1+I2)*S/N;
    sum(beta)*(I1+I2)*S/N - sigma*E;
    sigma*E - gamma1*I1;
    gamma1*I1 - gamma2*I2;
    gamma2*I2 - gamma3*W;
    z1*gamma3*W - alpha*H;
    z2*alpha*H - zeta*C;
    (1-z1)*gamma3*W + (1-z2)*alpha*H + zeta*C;
    z1*gamma3*W;
    sum(beta)*(I1+I2)*S/N;
];

end