%
% SCRIPT - SUPERSPREADER CASE EXAMPLE - SIMULATION
%
%   This script runs the superspreader case from [1]. It runs a simulation
%   based on the numbers computed with the fitting script. The plots from
%   [1] are generated using this.
%
% Code written by Bjørn Jensen for the COVID-19-CTRL joint project between
% The Technical University of Denmark (DTU) and Aalborg University (AAU).
%
% Copyright (c) 2020 Bjørn Jensen 
%
% [1] Efficient Uncertainty Quantification of  Predictions using Polynomial 
%     Chaos in Epidemic Modeling (2021) - Jensen B., Engsig-Karup A.P., 
%     Knudsen K.
%

close all
clear all
clc
% ========================================================================
% ========================================================================

% ------------------------------------------------------------------------
ss = 0.8;
ss_p = 0.1;
% -----------------------------------
% VALUES FROM 'FIT_PARAMETERS.M'
beta_sc = 0.601642;
restriction = {
    [1,0.130301, 0.186898, 0.188040],
    [16,46,86]
};
I0 = 473.571950;
% -----------------------------------
t = 0:180;

betafun = beta_time(ss,ss_p,beta_sc,restriction);

[beta,wg] = betafun(0);
disp('Mean R0')
disp(4.2*sum(beta.*wg))

tmp = beta.*wg;
disp('Superspreader contribution (%)')
disp(tmp(1)/sum(tmp))

disp('beta')
disp(beta)
for i = 1:numel(restriction{2})
    [beta,wg] = betafun(restriction{2}(i)+1);
    disp(4.2*sum(beta.*wg))
    disp(beta)
end
% ------------------------------------------------------------------------
X = run_model(t,betafun,I0);

% ------------------------------------------------------------------------
figure(11)
p = get(11,'Position');
set(11,'Position',[p(1)-p(3) p(2) p(3) .3*p(4) ])
plot(t,X(:,1),'LineWidth',2);
legend('S');
ylabel('individuals')
xlabel('days')
grid
box on
saveas(gcf,['img',num2str(get(gcf,'Number')),'.png'])

figure(12); hold on
set(12,'Position',[p(1)+p(3) p(2) p(3) .3*p(4) ])
T = t(1):t(end);
total_infected = interp1(t,X(:,10),T);
plot(T,total_infected,'LineWidth',2);
plot(t,X(:,8),'LineWidth',2);
legend('Total Infected','R');
ylabel('individuals')
xlabel('days')
grid
box on
saveas(gcf,['img',num2str(get(gcf,'Number')),'.png'])

figure(13)
set(13,'Position',[p(1) p(2)-p(4) p(3) .7*p(4) ])
plot(t,X(:,[2,3,4,5]),'LineWidth',2);
legend('E','I1','I2','W');
ylim([0,2e4])
ylabel('individuals')
xlabel('days')
grid
box on
saveas(gcf,['img',num2str(get(gcf,'Number')),'.png'])

figure(14)
set(14,'Position',[p(1)-p(3) p(2)-p(4) p(3) .2*p(4) ])
plot_restriction(t,restriction)
ylabel('openness')
xlabel('days')
grid 
box on
saveas(gcf,['img',num2str(get(gcf,'Number')),'.png'])

% ------------------------------------------------------------------------
figure(21); clf; hold on
set(21,'Position',[p(1) p(2) p(3) .7*p(4) ])
plot(t,X(:,[6,7]));

[Tn,D] = newly_hosp_data;
Tn = datenum(Tn);
Tn = Tn - Tn(1);
plot(Tn,D,'.','MarkerSize',20)

T = t(1):t(end);
total_admissions = interp1(t,X(:,9),T);
new_admissions_by_day = diff(total_admissions);
plot(T(2:end),new_admissions_by_day,':','LineWidth',3);
ylabel(['individuals'])

legend('H','C','Newly Admitted (data)','Newly Admitted (sim)');
ylim([0,450])

xlabel('days')
grid
box on
saveas(gcf,['img',num2str(get(gcf,'Number')),'.png'])


% ========================================================================
% ========================================================================
function X = testdesignfun(T,c3)
ss = 0.8;
ss_p = 0.1;
beta_sc = 0.601642;
restriction = {
    [1,0.130301, 0.186898, c3],
    [16,46,86]
};
I0 = 473.571950;

betafun = beta_time(ss,ss_p,beta_sc,restriction);

[beta,wg] = betafun(0);
disp('Mean R0')
disp(4.2*sum(beta.*wg))

tmp = beta.*wg;
disp('Superspreader contribution (%)')
disp(tmp(1)/sum(tmp))

disp('beta')
disp(beta)
for i = 1:numel(restriction{2})
    [beta,wg] = betafun(restriction{2}(i)+1);
    disp(4.2*sum(beta.*wg))
    disp(beta)
end
% ------------------------------------------------------------------------
X = run_model(T,betafun,I0);

end

% ------------------------------------------------------------------------
function [T,data] = newly_hosp_data

% WEBSITE DOESN'T EXIST ANYMORE!
% s = webread('https://api.covid19data.dk/ssi_newly_hospitalized');
% 
% T = {s.date}';
% T = datetime(T,'InputFormat','yyyy-MM-dd''T''hh:mm:''00+00:00''');
% data = [s.newly_hospitalized]';

tbl = readtable('Newly_admitted_over_time.csv');
T = tbl{:,1};
data = tbl{:,end};

end

% ------------------------------------------------------------------------
function plot_restriction(t,restriction)
F = pwfun(restriction{:});
plot(t,F(t),'r:','LineWidth',2);
for i = 1:length(restriction{2})
    text(restriction{2}(i),restriction{1}(1+i),sprintf(' %4.3f',restriction{1}(1+i)),...
          'VerticalAlign','bottom','HorizontalAlign','left');
end
ylim([0,1]);
end

% ------------------------------------------------------------------------
function b = beta_time(ss,p,sc_beta,restriction)
if nargin < 4, restriction = []; F = []; end
if ~isempty(restriction), F = pwfun(restriction{:}); end
ss_beta = ((1-p)/(1-ss))*(ss/p);
w = [p,1-p];
beta = ones(1, 2);
beta(1) = ss_beta;
mbeta = sum(beta.*w);
b = @betafun;
    function [b,wg] = betafun(t)
        sc = 1;
        if ~isempty(F), sc = F(t); end
        b = ones(1, 2);
        b(1) = ss_beta*sc;
        b(2) = min(1,ss_beta*sc);
        b = b*(sc_beta/mbeta);
        wg = w;
    end
end

% ------------------------------------------------------------------------
function f = pwfun(vals,jmppts)
    f = @fun;
    function y = fun(x)
        y = zeros(size(x));
        prejmp = -Inf;
        for i = 1:length(jmppts)
            jmp = jmppts(i);
            idx = x <= jmp & x > prejmp;
            y(idx) = vals(i);
            prejmp = jmp;
        end
        y(x > prejmp) = vals(end);
    end
end

% ------------------------------------------------------------------------
function X = run_model(T,betafun,I0)

% pde initial conditions and time span
N = 5.8e6;
X0 = [N-I0; I0/2; I0/3; I0/6; 0; 0; 0; 0; 0; I0];

% age group dynamics for hospital [10y groups 0-79 and 80+]
age_spread = [0.109, 0.119, 0.133, 0.117, 0.136, 0.136, 0.117, 0.089, 0.044];
prob_hosp = [0.00001, 0.00013, 0.0037, 0.011, 0.014, 0.027, 0.039, 0.055, 0.055];
prob_icu = [0.05, 0.05, 0.05, 0.05, 0.063, 0.122, 0.274, 0.432, 0.709];

% parameters
tau_incub = 1.2;
tau_presymp_inf = 1.2;
tau_inf = 3;
tau_wait = 2;
tau_hosp = 5;
tau_icu = 12;
sigma = 1/tau_incub;
gamma1 = 1/tau_presymp_inf;
gamma2 = 1/tau_inf;
gamma3 = 1/tau_wait;
alpha = 1/tau_hosp;
zeta = 1/tau_icu;
% Z1 :: Fraction of infected population getting hospitalized. This
%       calculation assumes that the age distribution of infected people is
%       close to the age distribution in society;
% Z2 :: Fraction of hospitalized population going into ICU. This uses the
%       derived age distribution for the hospitalized population: 
%          age_spread .* prob_hosp / z1
z1 = sum(age_spread.*prob_hosp); 
z2 = sum(age_spread.*prob_hosp.*prob_icu)/z1;

opts = {sigma,gamma1,gamma2,gamma3,z1,z2,alpha,zeta};

[t,X] = ode45(@model, T, X0, [], betafun, opts{:}, N);

end

% ------------------------------------------------------------------------
function dX = model(t,X,betafun,sigma,gamma1,gamma2,gamma3,z1,z2,alpha,zeta,N)
% SEIIWRHC-superspreaders
% 1:   dS(t) = -Int( beta(a) da=0..1)*(I1(t) + I2(t))*S(t)/N
% 2:   dE(t) = Int( beta(a) da=0..1)*(I1(t) + I2(t))*S(t)/N - sigma*E(t)
% 3:  dI1(t) = sigma*E(t) - gamma1*I1(t)
% 4:  dI2(t) = gamma1*I1(t) - gamma2*I2(t)
% 5:   dW(t) = gamma2*I2(t) - gamma3*W(t);
% 6:   dH(t) = z1*gamma3*W(t) - alpha*H(t)
% 7:   dC(t) = z2*alpha*H(t) - zeta*C(t)
% 8:   dR(t) = (1-z1)*gamma3*W(t) + (1-z2)*alpha*H(t) + zeta*C(t)
% Extra measurement compartments
% 10:  dNewlAmitted = z1*gamma3*W(t);
% 11:  dTotalInfected = Int( beta(a) da=0..1)*(I1(t) + I2(t))*S(t)/N
% 
[beta,wg] = betafun(t);
beta = beta.*wg;
X = num2cell(X);
[S,E,I1,I2,W,H,C,~] = X{:};
dX = [
    -sum(beta)*(I1+I2)*S/N;
    sum(beta)*(I1+I2)*S/N - sigma*E;
    sigma*E - gamma1*I1;
    gamma1*I1 - gamma2*I2;
    gamma2*I2 - gamma3*W;
    z1*gamma3*W - alpha*H;
    z2*alpha*H - zeta*C;
    (1-z1)*gamma3*W + (1-z2)*alpha*H + zeta*C;
    z1*gamma3*W;
    sum(beta)*(I1+I2)*S/N;
];

end