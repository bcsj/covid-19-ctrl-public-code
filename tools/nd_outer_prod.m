% A = ND_OUTER_PROD(V1,V2)
% A = ND_OUTER_PROD(V1,...,VN)
%
% ND_OUTER_PROD computes the outer product of N vectors producing an
% N-dimensional array A with elements a[i1,...,iN] given by
%
%   a[i1,...,iN] = v1[i1] * ... * vN[iN]
%
% Author information
%   Copyright (c) 2020 Bjørn Jensen
%
function A = nd_outer_prod(varargin)

n = cellfun(@(x) numel(x),varargin);
A = zeros(n);
sz = ones(1,numel(n));
ii = ones(1,numel(n));
S.type = '()';
S.subs = cell(1,numel(n));
for i = 1:prod(n)
    S.subs = mat2cell(ii,1,sz);
    val = prod(arrayfun(@(j) varargin{j}(ii(j)),1:numel(n)));
    A = subsasgn(A,S,val);
    ii = incr(ii,n);
end

end

function ii = incr(ii,M)
% multiindex incrementer
for i = 1:numel(M)
    ii(i) = ii(i) + 1;
    if i > 1, ii(i-1) = 1; end
    if ii(i) <= M(i), return, end
end
end