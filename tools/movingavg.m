% ARROUT = MOVINGAVG(ARRIN, WIDTH)
%
% MOVINGAVG computes a moving average of the input array ARRIN over a
% window of size WIDTH. The output array ARROUT has the same size as the
% input. At the end points the window is truncated.
%
% Author information
%   Copyright (c) 2020 Bjørn Jensen
%
function arrout = movingavg(arrin,width)
arrout = zeros(size(arrin));
for i = 1:numel(arrin)
    arrout(i) = mean(arrin([max(1,i-width):min(end,i+width)]));
end

end

