%
% MIT License
% 
% Copyright (c) 2020 Bjørn Jensen
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
classdef lognormal
    properties
        mu = 0;    % parameter #1
        sigma = 1; % parameter #2
    end
    
    methods
        function obj = lognormal(mu,sigma,prop)
            % OBJ = LOGNORMAL(MU, SIGMA)
            % OBJ = LOGNORMAL(MEAN, VAR, 'SetByMeanVar')
            % OBJ = LOGNORMAL([Q1,Q2], [X1,X2], 'SetByQuantiles')
            %
            % Inputs:
            %   MU - parameter #1
            %   SIGMA - parameter #2
            %   MEAN - mean value
            %   VAR - variance
            %   [Q1,Q2] - quantiles
            %   [X1,X2] - values corresponding to quantiles
            %
            if nargin > 0
                obj.mu = mu;
                obj.sigma = sigma;
                
                if nargin > 2 
                    if strcmpi(prop,'SetByMeanVar')
                        sig2 = log(sigma./mu.^2 + 1);
                        obj.sigma = sqrt(sig2);
                        obj.mu = log(mu) - sig2/2;
                        %log(mu.^2./sqrt(sigma + mu.^2));
                    elseif strcmpi(prop,'SetByQuantiles')
                        q1 = mu(1); q2 = mu(2);
                        x1 = sigma(1); x2 = sigma(2);
                        z1 = sqrt(2)*erfinv(2*q1-1);
                        z2 = sqrt(2)*erfinv(2*q2-1);
                        
                        s = (log(x2)-log(x1))./(z2 - z1);
                        obj.sigma = s;
                        obj.mu = log(x1) - s*z1;
                    end
                end
            end
        end
            
        function y = pdf(o,x)
            % probability distribution function
            m = o.mu; s = o.sigma;
            xi = (log(x) - m)/s;
            y = exp(-xi.^2/2)./(sqrt(2*pi)*s*x);
        end
        
        function p = cdf(o,x)
            % commulative distribution function
            m = o.mu; s = o.sigma;
            xi = (log(x) - m)/s;
            p = (1+erf(xi/sqrt(2)))/2;
        end
        
        function x = quantile(o,p)
            % quantile function
            m = o.mu; s = o.sigma;
            x = exp(m + s*sqrt(2)*erfinv(2*p-1));
        end
        
        function q = confidence_interval(o,p)
            q = [o.quantile((1-p)/2), o.quantile((1+p)/2)];
        end
        
        function mu = mean(o)
            m = o.mu; s = o.sigma;
            mu = exp(m + s.^2/2);
        end
        
        function sig = std(o)
            sig = sqrt(var(o));
        end
        
        function sig2 = var(o)
            m = o.mu; s = o.sigma;
            sig2 = (exp(s.^2) - 1) .* exp(2*m + s.^2);
        end
        
    end
end