%
% MIT License
% 
% Copyright (c) 2020 Bjørn Jensen
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
classdef exponential
    properties
        lambda = 1; % rate
    end
    
    methods
        function obj = exponential(lambda)
            % OBJ = EXPONENTIAL(LAMBDA)
            %
            % Inputs:
            %   LAMBDA - rate
            %
            if nargin > 0
                obj.lambda = lambda;
            end
        end
        
        function y = pdf(o,x)
            % probability distribution function
            y = o.lambda*exp(-o.lambda*x);
        end
        
        function p = cdf(o,x)
            % commulative distribution function
            p = 1-exp(-o.lambda*x);
        end
        
        function x = quantile(o,p)
            % quantile function
            x = -log(1-p)/o.lambda;
        end
        
        function q = confidence_interval(o,p)
            q = [o.quantile((1-p)/2), o.quantile((1+p)/2)];
        end
        
        function mu = mean(o)
            mu = 1/o.lambda;
        end
        
        function sig = std(o)
            sig = sqrt(var(o));
        end
        
        function sig2 = var(o)
            sig2 = 1/o.lambda^2;
        end
        
    end
end