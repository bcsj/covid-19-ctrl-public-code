%
% MIT License
% 
% Copyright (c) 2020 Bjørn Jensen
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
classdef uniform
    properties
        a = 0; % lower bound
        b = 1; % upper bound
    end
    
    methods
        function obj = uniform(a,b)
            % OBJ = UNIFORM(A, B)
            %
            % Inputs:
            %   A - lower bound
            %   B - upper bound
            %
            if nargin > 0
                obj.a = a;
                obj.b = b;
            end
        end
        
        function y = pdf(o,x)
            % probability distribution function
            y = ones(size(x))/(o.b-o.a);
        end
        
        function p = cdf(o,x)
            % commulative distribution function
            p = (x-o.a)/(o.b-o.a);
        end
        
        function x = quantile(o,p)
            % quantile function
            x = p*(o.b-o.a) + o.a;
        end
        
        function q = confidence_interval(o,p)
            q = [o.quantile((1-p)/2), o.quantile((1+p)/2)];
        end
        
        function mu = mean(o)
            mu = (o.a+o.b)/2;
        end
        
        function sig = std(o)
            sig = sqrt(var(o));
        end
        
        function sig2 = var(o)
            sig2 = (o.b-o.a).^2/12;
        end
        
    end
end