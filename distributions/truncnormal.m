%
% MIT License
% 
% Copyright (c) 2020 Bjørn Jensen
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
classdef truncnormal
    properties
        mu = 0;    % parameter #1
        sigma = 1; % parameter #2
        a = -Inf;    % lower truncation bound
        b = Inf;     % upper truncation bound
    end
    
    methods
        function obj = truncnormal(mu,sigma,a,b,prop)
            % OBJ = TRUNCNORMAL(MU, SIGMA, A, B)
            % OBJ = TRUNCNORMAL(MEAN, VAR, A, B, 'SetByMeanVar')
            %
            % Inputs:
            %   MU - mean value
            %   SIGMA - standard deviation
            %   A - lower bound truncation
            %   B - upper bound truncation
            %   MEAN - mean value
            %   VAR - variance
            %
            if nargin > 0
                obj.mu = mu;
                obj.sigma = sigma;
                obj.a = a;
                obj.b = b;
                
                if nargin > 4
                    if strcmpi(prop,'SetByMeanVar')
                        mean_ = mu;
                        var_ = sigma;
                        mu = zeros(length(mean_),1);
                        sig = zeros(length(mean_),1);
                        
                        options = optimoptions('fmincon', ...
                            'Algorithm','interior-point', ...
                            'display','none', ...
                            'MaxFunctionEvaluations',3e3, ...
                            'OptimalityTolerance', 1e-10, ...
                            'StepTolerance', 1e-10, ...
                            'MaxIterations',1e3);
                        lb = [a, 0];
                        ub = [b, Inf];
                        flag = false;
                        for i = 1:length(mean_)
                            initVals = [mean_(i), var_(i)];
                            [p,fval] = fmincon(@(p) obj.mean_var_wrapper(p(1),p(2),a,b,mean_(i),var_(i)), ...
                                initVals,[],[],[],[],lb,ub, [],options);
                            mu(i) = p(1);
                            sig(i) = p(2);
                            if fval > 1e-6
                                flag = true;
                            end
                        end
                        obj.mu = mu;
                        obj.sigma = sig;
                        if flag
                            warning('Not all means and variances could be reached')
                        end
                    end
                end
            end
        end
        
        function y = pdf(o,x)
            % probability distribution function
            m = o.mu; s = o.sigma;
            xi = (x - m)./s;
            y = truncnormal.normal_pdf(xi)./(s.*o.Z);
        end
        
        function p = cdf(o,x)
            % commulative distribution function
            m = o.mu; s = o.sigma;
            xi = (x - m)./s;
            p = (truncnormal.normal_cdf(xi) - truncnormal.normal_cdf(o.alpha))./o.Z;
        end
        
        function x = quantile(o,p)
            % quantile function
            m = o.mu; s = o.sigma;
            PA = truncnormal.normal_cdf(o.alpha);
            x = m + sqrt(2)*s.*erfinv(2.*(p.*o.Z + PA)-1);
        end
        
        function q = confidence_interval(o,p)
            q = [o.quantile((1-p)/2), o.quantile((1+p)/2)];
        end
        
        function mu = mean(o)
            mu = o.mu + o.sigma.*(o.pdfA - o.pdfB)./o.Z;
        end
        
        function sig = std(o)
            sig = sqrt(var(o));
        end
        
        function sig2 = var(o)
            s = o.sigma;
            sig2 = s.^2.*(1 + (o.ApdfA - o.BpdfB)./o.Z - ((o.pdfA - o.pdfB)./o.Z).^2);
        end
        
    end
    
    methods (Static)
        function y = normal_pdf(x)
            y = exp(-x.^2/2)/sqrt(2*pi);
        end
        
        function p = normal_cdf(x)
            p = (1+erf(x/sqrt(2)))/2;
        end
    end
    
    methods (Static, Hidden)
        % Methods for estimating the parameters mu and sigma given
        % desired mean and variances.
        function v = mean_var_wrapper(mu,sigma,a,b,m,va)
            [m_est,va_est] = truncnormal.mean_var_forward(mu,sigma,a,b);
            v = sqrt( (m_est-m).^2 + (va_est-va).^2 );
        end
        
        function [m,va] = mean_var_forward(mu,sigma,a,b)
            s = sigma;
            alpha = (a - mu)./sigma;
            beta = (b - mu)./sigma;
            if a == -Inf
                pdfA = 0;
                cdfA = 0;
                ApdfA = 0;
            else
                pdfA = truncnormal.normal_pdf(alpha);
                cdfA = truncnormal.normal_cdf(alpha);
                ApdfA = alpha.*pdfA;
            end
            if b == Inf
                pdfB = 0;
                cdfB = 1;
                BpdfB = 0;
            else
                pdfB = truncnormal.normal_pdf(beta);
                cdfB = truncnormal.normal_cdf(beta);
                BpdfB = beta.*pdfB;
            end
            Z = cdfB - cdfA;
            m = mu + s.*(pdfA - pdfB)./Z;
            va = s.^2.*(1 + (ApdfA - BpdfB)./Z - ((pdfA - pdfB)./Z).^2);
        end
    end
    
    methods (Hidden)
        function v = alpha(o)
            v = (o.a - o.mu)./o.sigma;
        end
        
        function v = beta(o)
            v = (o.b - o.mu)./o.sigma;
        end
        
        function z = Z(o)
            z = o.cdfB - o.cdfA;
        end
        
        function v = pdfA(o)
            if o.a == -Inf
                v = 0;
            else
                v = truncnormal.normal_pdf(o.alpha);
            end
        end
        
        function v = pdfB(o)
            if o.b == Inf
                v = 0;
            else
                v = truncnormal.normal_pdf(o.beta);
            end
        end
        
        function v = ApdfA(o)
            if o.a == -Inf
                v = 0;
            else
                v = o.alpha.*truncnormal.normal_pdf(o.alpha);
            end
        end
        
        function v = BpdfB(o)
            if o.b == Inf
                v = 0;
            else
                v = o.beta.*truncnormal.normal_pdf(o.beta);
            end
        end
        
        function v = cdfA(o)
            if o.a == -Inf
                v = 0;
            else
                v = truncnormal.normal_cdf(o.alpha);
            end
        end
        
        function v = cdfB(o)
            if o.b == Inf
                v = 1;
            else
                v = truncnormal.normal_cdf(o.beta);
            end
        end       
        
    end
end