%
% MIT License
% 
% Copyright (c) 2020 Bjørn Jensen
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
classdef normal
    properties
        mu = 0;    % mean
        sigma = 1; % standard deviation
    end
    
    methods
        function obj = normal(mu,sigma,prop)
            % OBJ = NORMAL(MU, SIGMA)
            % OBJ = NORMAL([Q1,Q2], [X1,X2],'SetByQuantiles')
            %
            % Inputs:
            %   MU - mean value
            %   SIGMA - standard deviation
            %   [Q1,Q2] - quantiles
            %   [X1,X2] - values corresponding to quantiles
            %
            if nargin > 0
                obj.mu = mu;
                obj.sigma = sigma;
                
                if nargin > 2 && strcmpi(prop,'SetByQuantiles')
                    q1 = mu(1); q2 = mu(2);
                    x1 = sigma(1); x2 = sigma(2);
                    z1 = sqrt(2)*erfinv(2*q1-1);
                    z2 = sqrt(2)*erfinv(2*q2-1);
                    
                    s = (x2-x1)/(z2 - z1);
                    obj.sigma = s;
                    obj.mu = x1 - s*z1;
                end
            end
        end
        
        function y = pdf(o,x)
            % probability distribution function
            m = o.mu; s = o.sigma;
            xi = (x - m)/s;
            y = exp(-xi.^2/2)/(sqrt(2*pi)*s);
        end
        
        function p = cdf(o,x)
            % commulative distribution function
            m = o.mu; s = o.sigma;
            xi = (x - m)/s;
            p = (1+erf(xi/sqrt(2)))/2;
        end
        
        function x = quantile(o,p)
            % quantile function
            m = o.mu; s = o.sigma;
            x = m + s*sqrt(2)*erfinv(2*p-1);
        end
        
        function q = confidence_interval(o,p)
            q = [o.quantile((1-p)/2), o.quantile((1+p)/2)];
        end
        
        function mu = mean(o)
            mu = o.mu;
        end
        
        function sig = std(o)
            sig = o.sigma;
        end
        
        function sig2 = var(o)
            sig2 = o.sigma.^2;
        end
        
    end
end