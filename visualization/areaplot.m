% AREAPLOT(YU, ...)
% AREAPLOT(YLU, ...)
% AREAPLOT(X, YU, ...)
% AREAPLOT(X, YLU, ...)
% AREAPLOT(X, YU, YL, ...)
% AREAPLOT(..., 'PropertyName', PropertyValue)
% AREAPLOT(AX, ...)
% H = AREAPLOT(...)
%
% AREAPLOT fills out the area between the curves YU(X) and YL(X). If YL(X)
% is not supplied it is assumed to be zero. If X is not supplied it is
% assumed to be 1:LENGTH(YU).
% If YU is N-x-2 it will be treated as YLU with YL in column 1 and YU in
% column 2.
%
% Properties are the PATCH PROPERTIES.
%
% H is a PATCH handle with TAG set as 'areaplot'.
% 
% See also fill, patch

%
% MIT License
% 
% Copyright (c) 2020 Bjørn Jensen
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
function h = areaplot(varargin)

% Check if first argument is an axes handle
v1 = varargin{1};
if numel(v1) == 1 && ishandle(v1) && strcmp(get(v1,'Type'),'axes')
    ax = v1;
    varargin = varargin(2:end);
else
    ax = gca;
end

% Manage visual parameters
idx = cellfun(@(x) ischar(x), varargin);
if ~any(idx)
    n = numel(idx);
else
    n = find(idx,1)-1;
end
switch (n)
    case 1
        yu = varargin{1};
        x = 1:length(yu);
        yl = zeros(size(yu));
    case 2
        x = varargin{1};
        yu = varargin{2};
        yl = zeros(size(yu));
    case 3
        x = varargin{1};
        yu = varargin{2};
        yl = varargin{3};
end
if min(size(yu)) == 2
    if size(yu,1) == 2 && size(yu,2) ~= 2
        yu = yu';
    end
    yl = yu(:,1);
    yu = yu(:,2);
end
    
varargin = varargin(n+1:end);

X = [x(:); flipud(x(:))];
Y = [yu(:); flipud(yl(:))];

% Handle color and linestyle specifically
if ~isempty(varargin) && any(strcmpi(varargin,'Color'))
    idx = strcmpi(varargin,'Color');
    idy = circshift(idx,1);
    color = varargin{idy};
    varargin = varargin(~(idx | idy));
else
    idx = numel(findall(ax,'Tag','areaplot'));
    color = color_cycle(idx+1);
end

if ~isempty(varargin) && any(strcmpi(varargin,'LineStyle'))
    idx = strcmpi(varargin,'LineStyle');
    idy = circshift(idx,1);
    ls = varargin{idy};
    varargin = varargin(~(idx | idy));
else
    ls = 'None';
end

% Plot patch
h = fill(X,Y,color,'LineStyle',ls,varargin{:});

% Set tag, used for cycling through colors
set(h,'tag','areaplot')

end

% Color function
function col = color_cycle(i)
i = mod(i-1,9)+1;
base = 1;
comp = .35;
col = zeros(1,3);
ii = mod(i-1,3)+1;
jj = floor((i-1)/3)+1;
col = [base,(2-jj+1)*comp,(jj-1)*comp];
col = circshift(col,ii);
end